# NOTAS CLASE DE HOY

## REPLICAR PROYECTO EN CASA:
- clonar repo
- cp .env.example .env
- composer install
- php artisan key:generate
- php artisan migrate:refresh --seed
- php artisan serve






## HEMOS VISTO:

- Migraciones:
  -  php artisan migrate
  -  php artisan migrate:rollback
  -  php artisan db:seed
  -  php artisan migrate:refresh
  -  php artisan migrate:refresh --seed

- Seeders
    -    Hemos creado seeders
    -    DataBaseSeeder es la raiz para ejecutarlos
    -    Hemos usado $faker y Model Factories (database/factories/)

-    Tests:
  -  test sobre la lista de usuarios
  -  test sobre el detalle del usuario 1.

- NOTA:
    alias sobre comando:

    alias t=vendor/bin/phpunit









