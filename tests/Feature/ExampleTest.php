<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_visita_de_bienvenida()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSee('Clase de Laravel');

    }
}
